/*
* Script for product page
* */
(function ($) {
    "use strict";

    // Change browser url
    function __change_url(url) {
        window.history.pushState("data", "Title", url);
        document.title = url;
    }

    // Get URL query string
    function __getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    // Remove duplicate value on array
    function __removeDumplicateValue(myArray) {
        var newArray = [];

        $.each(myArray, function (key, value) {
            var exists = false;
            $.each(newArray, function (k, val2) {
                if (value.id === val2.id) {
                    exists = true
                }
            });
            if (exists === false && value.id !== "") {
                newArray.push(value);
            }
        });

        return newArray;
    }

    // Get product data from hidden field on product page
    const product = function () {
        let data = $('#product-js-data').text();
        return data ? JSON.parse(data) : false;
    };

    // Currency settings
    const currency = new Intl.NumberFormat('en-US', {
        style   : 'currency',
        currency: 'USD',
    });

    // Product colors choose (event)
    function product_colors_select() {

        $(document).on('click', '.product-colors input[type=radio]', function () {

            // Check if color radio button is checked
            if ($(this).is(':checked')) {

                let name = $(this).val();
                let header = $('.product-color-header');
                let color = header.find('.color-name');

                // Show the header
                header.show(0);

                // Append selected color name
                color.text(name);

                // Get product price
                let price = get_product_price(name);
                if (price.length !== 0) {
                    $('.product-price')
                        .show(0)
                        .find('.format-price')
                        .text(currency.format(price));
                } else {
                    console.log('Product price does not exit');
                }

                // Get compare price
                let compare_price = get_compare_price(name);
                if (compare_price) {
                    $('.product-price-compare')
                        .show(0)
                        .find('.format-price')
                        .text(currency.format(compare_price));
                }

                // Check size is selected
                check_size();

                // Get sizes
                let sizes = get_sizes(name);
                if (sizes.length !== 0) {

                    // Disable sizes by default
                    let sizes_list = $('.sizes-list');
                    sizes_list.find('input')
                        .prop('checked', false)
                        .attr('disabled', 'disabled');
                    sizes_list.find('.size-disable').show(0);

                    // Enable sizes according to color match
                    $.each(sizes, function (index, size) {
                        let field = $('.sizes-list').find('#size-' + size + '');
                        field.find('.size-disable').hide(0);
                        field.find('input').removeAttr('disabled');
                    });

                    // If previous sizes is selected then remain selected on another (color) swatch
                    let existing_size = $('#product-swatches').attr('data-selected-size');
                    if (existing_size) {
                        let prev_size = sizes_list.find('#size-' + existing_size + '').first();
                        if (prev_size.find('.size-disable').is(':hidden')) {
                            prev_size.find('input').prop('checked', true).trigger('click');
                        }
                    }

                } else {
                    console.log('Sizes does not exist');
                }

                // Call buy button
                buy_button_activate();

                // Get image id by color
                let variant_images = get_variants(name);
                if (variant_images.length !== 0) {
                    let image_id_by_color = 0;
                    $.each(variant_images, function (index, item) {
                        if (((item.featured_image || {}).id || 0)) {
                            image_id_by_color = item.featured_image.id;
                            return false;
                        }
                    });

                    // Scroll to slide according to color
                    scroll_to_slide(image_id_by_color, $('.product-carousel'), true);
                } else {
                    console.log('Variant does not exist');
                }

                // On slider display images according to color
                display_images_by_color(name);

                // Add variant id on url
                add_variant_on_url(name, variant_images);

                // Show sizes
                $('.product-sizes').find('.sizes-heading, .sizes-list').addClass('active');

                // Show slider
                let product_slider = $('.product-carousel');
                product_slider.find('.carousel-slide').addClass('active');
                $('.product-carousel-nav').addClass('active');
                product_slider.addClass('active');
                $(".product-carousel").css('opacity', '1');
            }
        });
    }

    // Price update according to color and size (event)
    function size_event_for_price_update() {

        $(document).on('click', '.sizes-list input[type=radio]', function (event) {

            // Check if color radio button is checked
            if ($(this).is(':checked')) {

                // For gift card product without colors
                if (colors_exist() === 0) {

                    // Enable buy button
                    let buy_button = $('#product-cart-btn');
                    let enable_label = buy_button.attr('data-enable');
                    buy_button.removeClass('btn-disable').text(enable_label);

                    // Enable PayPal button
                    PayPal_button(true);

                    // Get size id
                    let sizes = $('.sizes-list').find('input:checked');
                    let get_size_id = sizes.attr('data-size-id');

                    // Assign variant id for form
                    $('#form-variant-id').val(get_size_id);

                    // Slider images
                    display_images_by_gift_card(get_size_id);

                    // Scroll to slide according to color
                    let get_image_id = sizes.attr('data-size-img-id');
                    scroll_to_slide(get_image_id, $('.product-carousel'), true);

                    let variants = product().variants;
                    let cursize = $(this).val();
                    $.map(variants, function (item) {
                        if (item.title == cursize) {
                            let price = item.price / 100;
                            $('.product-price')
                                .show(0)
                                .find('.format-price')
                                .text(currency.format(price));
                        }
                    });

                } else {

                    let color = $('.product-colors').find('input:checked').val();
                    let size = $(this).val();

                    // Price update according to color & size
                    let price = get_product_price(color, size);
                    if (price.length !== 0) {
                        $('.product-price')
                            .show(0)
                            .find('.format-price')
                            .text(currency.format(price));
                    } else {
                        console.log('Product price does not exit');
                    }

                    // Compare price update according to color & size
                    let compare_price = get_compare_price(color, size);
                    if (compare_price) {
                        $('.product-price-compare')
                            .show(0)
                            .find('.format-price')
                            .text(currency.format(compare_price));
                    }

                    // Call buy button
                    buy_button_activate();

                    // Reset variant id input field
                    let form_variant_input = $('#add-to-cart-form').find('#form-variant-id');
                    form_variant_input.val(0);

                    // Get image id by color & size
                    let variant_images = get_variants(color, size);
                    if (variant_images.length !== 0) {
                        let variant_id = 0;
                        let image_id_by_color = 0;
                        $.each(variant_images, function (index, item) {
                            if (item.id) {
                                variant_id = item.id;
                                if (((item.featured_image || {}).id || 0)) {
                                    image_id_by_color = item.featured_image.id;
                                }
                                return false;
                            }
                        });

                        // Scroll to slide according to color & size
                        // scroll_to_slide(image_id_by_color);

                        // Id update on (Add to cart) button form
                        if (variant_id !== 0) {
                            form_variant_input.val(variant_id);
                        }
                    } else {
                        console.log('Variant does not exist');
                    }
                }
            }
        });
    }

    // Get product price according to color
    function get_product_price(color, size) {

        let variants = product().variants;
        return variants ? $.map(variants, function (item) {
            if (size) {
                if (item.option2 === color && item.option1 === size && item.available === true) {
                    // Cents convert into dollar
                    return item.price / 100;
                }
            } else {
                if (item.option2 === color && item.available === true) {
                    // Cents convert into dollar
                    return item.price / 100;
                }
            }
        })[0] : 0;
    }

    // Get compare price according to color
    function get_compare_price(color, size) {

        let variants = product().variants;
        return variants ? $.map(variants, function (item) {
            if (size) {
                if (item.option2 === color && item.option1 === size && item.available === true) {
                    // Cents convert into dollar
                    return item.compare_at_price / 100;
                }
            } else {
                if (item.option2 === color && item.available === true) {
                    // Cents convert into dollar
                    return item.compare_at_price / 100;
                }
            }
        })[0] : 0;
    }

    // Get sizes according to color
    function get_sizes(color) {
        let variants = product().variants;
        return variants ? $.map(variants, function (item) {
            // Get size by color name & available
            if (item.option2 === color && item.available === true) {
                return item.option1;
            }
        }) : false;
    }

    // Check size is selected
    function check_size() {

        let size = $('.product-sizes').find('input:checked').val();

        if (!size) {
            return false;
        }

        // Set selected size as attribute
        $('#product-swatches').attr('data-selected-size', size);

        return size;
    }

    // Activate buy button when color & size is selected
    function buy_button_activate() {

        let color = $('.product-colors').find('input:checked').val();
        let size = $('.product-sizes').find('input:checked').val();
        let buy_button = $('#product-cart-btn');

        // Stop if color & size does not exist
        if (!(color && size)) {

            // Disable buy button
            let disable_label = buy_button.attr('data-disable');
            buy_button.addClass('btn-disable').text(disable_label);

            // Disable PayPal button
            PayPal_button(false);

            return false;
        }

        // Enable buy button
        let enable_label = buy_button.attr('data-enable');
        buy_button.removeClass('btn-disable').text(enable_label);

        // Enable PayPal button
        PayPal_button(true);
    }

    // On slider display images according to color
    function display_images_by_color(color, selector) {
        let variant_images = get_variants(color);
        if (variant_images.length !== 0) {
            let images_unique = $.map(variant_images, function (item) {
                if (((item.featured_image || {}).id || 0)) {
                    return item.featured_image;
                }
            });
            images_unique = __removeDumplicateValue(images_unique);
            if (images_unique) {
                let slider = $('.product-carousel, .product-carousel-nav');
                if (selector) {
                    slider = selector;
                }
                slider.find('.carousel-slide').remove();
                $.each(images_unique, function (index, item) {
                    slider.slick('slickAdd', '<div class="carousel-slide" data-image-id="' + item.id + '">' +
                        '<div class="slide-overlay"></div>' +
                        '<div class="image-wrapper">' +
                        '<img src="' + item.src + '" alt="' + item.alt + '">' +
                        '</div>' +
                        '</div>');
                });
            }
        }
    }

    //  On slider display images without color
    function display_images_by_gift_card(variant_id, selector) {
        let variants = product().variants;
        if (variants.length !== 0) {
            let images_unique = $.map(variants, function (item) {
                if (((item.featured_image || {}).id || 0)) {
                    return item.featured_image;
                }
            });
            images_unique = __removeDumplicateValue(images_unique);
            if (images_unique) {
                let slider = $('.product-carousel, .product-carousel-nav');
                if (selector) {
                    slider = selector;
                }
                slider.find('.carousel-slide').remove();
                $.each(images_unique, function (index, item) {
                    slider.slick('slickAdd', '<div class="carousel-slide" data-image-id="' + item.id + '">' +
                        '<div class="slide-overlay"></div>' +
                        '<div class="image-wrapper">' +
                        '<img src="' + item.src + '" alt="' + item.alt + '">' +
                        '</div>' +
                        '</div>');
                });
            }
        }

        // Show slider
        let product_slider = $('.product-carousel');
        product_slider.find('.carousel-slide').addClass('active');
        $('.product-carousel-nav').addClass('active');
        product_slider.addClass('active');
        $(".product-carousel").css('opacity', '1');
    }

    // Product slider
    function product_slider() {

        let product_slider = $('.product-carousel');

        product_slider.slick({
            speed         : 400,
            infinite      : true,
            centerMode    : false,
            dots          : true,
            slidesToShow  : 1,
            slidesToScroll: 1,
            asNavFor      : '.product-carousel-nav',
            responsive    : [
                {
                    breakpoint: 767,
                    settings  : {
                        dots: true
                    }
                }
            ]
        });

        // Slider show
        product_slider.on('afterChange', function () {
            product_slider.addClass('slider-show');
            $('.product-carousel-loader').fadeOut(800);
        });

        $('.product-carousel-nav').slick({
            slidesToShow  : 4,
            slidesToScroll: 1,
            infinite      : true,
            asNavFor      : '.product-carousel',
            dots          : false,
            centerMode    : false,
            focusOnSelect : true
        });
    }

    // Scroll to slider when user select the color and size
    function scroll_to_slide(image_id, selector, force) {
        if (image_id) {
            let slider = $('.product-carousel');
            if (selector) {
                slider = selector;
            }
            if (slider.find('.slick-list').length !== 0) {
                let slide_index = slider.find('[data-image-id="' + image_id + '"]').eq(0)
                    .attr('data-slick-index');
                slider.slick('slickGoTo', slide_index, force);
            }
        }
    }

    // Colors exist
    function colors_exist() {
        let variants = product().variants;
        let color_length = $.map(variants, function (item) {
            return item.option2;
        }).length;
        return color_length ? color_length : 0;
    }

    // Get variants according to color & size
    function get_variants(color, size) {
        let variants = product().variants;
        return variants ? $.map(variants, function (item) {
            if (size) {
                // if (item.option2 === color && item.option1 === size && item.featured_image && item.available === true) {
                if (item.option2 === color && item.option1 === size && item.available === true) {
                    return item;
                }
            } else {
                // if (item.option2 === color && item.featured_image && item.available === true) {
                if (item.option2 === color && item.available === true) {
                    return item;
                }
            }
        }) : false;
    }

    // Select first variation (color) on load
    function color_select_on_load() {

        let field = $('.product-colors');
        let color = field.find('input:checked').val();

        // Get variant id from url
        let url_variant = __getUrlVars()['variant'];
        if (url_variant) {
            url_variant = parseFloat(url_variant);
            let color_name = '';
            let variants = product().variants;
            $.each(variants, function (index, item) {
                if (item.id && item.id === url_variant) {
                    color_name = item.option2;
                    return false;
                }
            });
            if (color_name) {
                // Color select according to variant id
                field.find('input[value="' + color_name + '"]').trigger('click');
                console.log(color_name + ': color is selected');
            } else {
                // Select first variation (color)
                if (color === undefined) {
                    field.find('input').eq(0).trigger('click');
                }
            }
        } else {

            // Select first variation (color)
            if (color === undefined) {
                field.find('input').eq(0).trigger('click');
            }
        }
    }

    // Add to cart using ajax
    function add_to_cart() {

        // Form submit
        $(document).on('submit', '#add-to-cart-form', function (event) {
            event.preventDefault();

            let submit_btn = $(this).find('#product-cart-btn');

            // Stop if button is disable
            if (submit_btn.hasClass('btn-disable')) {
                return false;
            }

            // For gift card product
            if (colors_exist() !== 0) {

                let color = $('.product-colors').find('input:checked').val();
                let size = $('.product-sizes').find('input:checked').val();

                // Stop if color & size does not exit
                if (!(color && size)) {
                    return false
                }
            }

            // Disable button before ajax start
            submit_btn.addClass('btn-disable').text('Added');

            let data = $(this).serialize();
            let add_to_cart = $.ajax({
                type    : "POST",
                url     : '/cart/add.js',
                data    : data,
                dataType: 'json'
            });

            $.when(add_to_cart).then(function (response) {
                if (response.hasOwnProperty('id')) {
                    get_cart(submit_btn);
                } else {
                    console.log(response);
                }
            }, function () {
                alert('Cart Error');
            });
        });
    }

    // Get cart data after add to cart
    function get_cart(submit_btn) {
        let get_cart = $.getJSON('/cart.js');
        $.when(get_cart).then(function (response) {
            if (response.hasOwnProperty('item_count')) {

                // Update cart
                refreshCart(response);

                // Enable button
                let enable_label = submit_btn.attr('data-enable');
                submit_btn.removeClass('btn-disable').text(enable_label);

                // Click on cart icon button
                document.getElementById('add-to-cart-icon').click();

            } else {
                console.log(response);
            }
        });
    }

    // Popup slider
    function popup_slider() {

        // Close popup when click on popup container
        $(document).on('click', '.product-popup', function (event) {
            event.preventDefault();

            if (event.target !== event.currentTarget) {
                return false;
            }

            // Hide
            $(this).stop(true, true).fadeOut(200, function () {

                // Destroy slider
                $('.product-carousel-popup').slick('unslick');
            });
        });

        // Close popup when click on close button
        $(document).on('click', '.product-popup-inner .fancybox-close', function (event) {
            event.preventDefault();
            $('.product-popup').trigger('click');
        });

        // Popup show
        $(document).on('click', '.product-carousel .carousel-slide', function () {

            // Get image id
            let image_id = $(this).attr('data-image-id');

            let popup = $('.product-carousel-popup');

            // Initialize slider
            popup.slick({
                infinite      : true,
                centerMode    : false,
                slidesToShow  : 1,
                slidesToScroll: 1,
            });

            let color = $('.product-colors').find('input:checked').val();

            // On slider display images according to color
            display_images_by_color(color, popup);

            // Scroll to active slide
            scroll_to_slide(image_id, popup, true);

            // Show popup
            $('.product-popup').stop(true, true).fadeIn(200);
        });
    }

    // Add variant id on url
    function add_variant_on_url(color, variant_images) {
        if (variant_images.length !== 0) {
            let variant_id = 0;
            $.each(variant_images, function (index, item) {
                if (item.id) {
                    variant_id = item.id;
                    return false;
                }
            });
            if (variant_id) {
                // Get product url
                let product_url = $('#product-url').val();
                if (product_url) {
                    // Change browser url and add variant id
                    __change_url(product_url + '?variant=' + variant_id);
                }
            }
        }
    }

    // PayPal / Buy it now
    function PayPal_button(enable) {
        let form = $('#add-to-cart-form');
        let button = form.find('.shopify-payment-button');
        if (enable === true) {
            button.addClass('active');
        } else {
            button.removeClass('active');
        }
    }

    // Gift card without colors
    function gift_card_without_colors() {

        let length = colors_exist();

        if (length === 0) {
            $('#product-swatches').remove();
            // $('.product-sizes .sizes-heading').show(0);
            $('.product-sizes .sizes-list').show(0);

            // Get all variants
            let variants = product().variants;

            //  Available variants
            $.map(variants, function (item) {
                if (item.available === true) {
                    let sizes = $('.sizes-list');
                    let input = sizes.find('[value="' + item.title + '"]');
                    let image_id = 0;
                    if (((item.featured_image || {}).id || 0)) {
                        image_id = item.featured_image.id;
                    }
                    input.attr('data-size-id', item.id)
                        .attr('data-size-img-id', image_id)
                        .removeAttr('disabled');
                    input.closest('li').find('.size-disable').remove();
                }
            });

            $('.sizes-list').find('input').eq(0).trigger('click');
        }
    }

    // Initialize
    let url_variant = __getUrlVars()['variant'];
    if (url_variant) {
        console.log('check variant');
        $(".product-carousel").css({
            'opacity'   : '0',
            'min-height': '253px'
        });
    }
    product_colors_select();
    size_event_for_price_update();
    add_to_cart();
    $(window).on('load', function () {
        product_slider();
        popup_slider();
        color_select_on_load();
        gift_card_without_colors();
    });

})(jQuery, document, window);
