var themeExtensions = {
  initTabs: function() {
    if ($('.tabs').length == 0) return;

    $('.tabs .tabs-nav a').on('click', function(evt) {
      evt.preventDefault();

      $('.tabs .tabs-nav li').removeClass('active');
      $(this).closest('li').addClass('active');
      $('.tabs .tab-content').removeClass('active');
      $('.tabs > ' + $(this).attr('href')).addClass('active');
    });

    $('.tabs').each(function() {
      $(this).find('.tabs-nav a').first().trigger('click');
    });
  },

  initTableWrapper: function() {
    $('table').wrap('<div class="table-wrapper">');
  },


  flashTimer: null,
  flashCart: function() {
    if (this.flashTimer != null) {
      clearTimeout(this.flashTimer);
    }

    $('.main-menu .cart-text-link').removeClass('fade');
    $('.main-menu .cart-text-link a').css('color', 'red');
    this.flashTimer = setTimeout(function() {
      $('.main-menu .cart-text-link').addClass('fade');

      clearTimeout(this.flashTimer);
      this.flashTimer = setTimeout(function() {
        $('.main-menu .cart-text-link a').css('color', '');

        clearTimeout(this.flashTimer);
        this.flashTimer = setTimeout(function() {
          $('.main-menu .cart-text-link').removeClass('fade');
        }, 3000);
      }, 1000);
    }, 10);
  },

  popupTimer: null,
  showCartBubble: function(product) {
    // console.log(product);
    $('#add-to-cart-popup img').attr('src', product.image);
    var title = (product.variant_title && product.variant_title != 'Default Title') ? product.product_title + '<small>' + product.variant_title + '</small>' : product.product_title;
    $('#add-to-cart-popup span.title').html(title);
    clearTimeout(this.popupTimer);
    $('#add-to-cart-popup').stop();
    $('#add-to-cart-popup')[0].style = '';
    $('#add-to-cart-popup').addClass('visible');
    if (product.quantity > 1) {
      $('#add-to-cart-popup .message').text(product.quantity + ' items in cart');
    } else {
      $('#add-to-cart-popup .message').text(product.quantity + ' item in cart');
    }
    this.popupTimer = setTimeout(this.hideCartBubble, 8000);
  },

  hideCartBubble: function() {
    clearTimeout(this.popupTimer);
    $('#add-to-cart-popup').stop().animate({opacity: 0}, 1000, function() {
      setTimeout(function() {
        $('#add-to-cart-popup').removeClass('visible');
        $('#add-to-cart-popup img').attr('src', '');
        $('#add-to-cart-popup span.title').text('');
        $('#add-to-cart-popup')[0].style = '';
      }, 1000);
    });
  },

  initMenuSearch: function() {
    var search = $('.site-header .header-search.standalone').detach();
    $('.nav--desktop .main-menu .search_link').append(search);

    $('.nav--desktop .main-menu .search_link a').on('click', function(evt) {
      evt.preventDefault();
      $('.site-header .header-search.standalone').toggleClass('visible');
      $('.site-header .header-search.standalone input[type=search]').focus();
    });

    $('.nav--desktop .main-menu .search_link .header-search .search-btn').on('click', function(evt) {
      evt.preventDefault();
      $('.site-header .header-search.standalone input[type=search]').focus();
    });


    $('main').on('click', function(evt) {
      if ($(evt.target).closest('.search_link').length == 0) {
        $('.site-header .header-search.standalone').removeClass('visible');
      }
    });
  },

  initThumbPopups: function() {
    if ($('#ProductThumbs-product .color-popup').length == 0) {
      $('<span class="color-popup"></span>').appendTo('#ProductThumbs-product');
    }

    $(document).on('mouseenter', '#ProductThumbs-product img', function(evt) {
      if ($(this).data('variant-color')) {
        $('.color-popup').text($(this).data('variant-color')).appendTo($(this).closest('li'));
      } else {
        $('.color-popup').appendTo('#ProductThumbs-product');
      }
    });
    $(document).on('mouseleave', '#ProductThumbs-product img', function(evt) {
      $('.color-popup').appendTo('#ProductThumbs-product');
    });
  },



  initProductVideo: function() {
    if ($('.product-description .video-wrapper iframe').length == 0) return;

    var fullHTML = '';
    var prependHTML = '';
    $('.product-description .video-wrapper iframe').each(function() {
      var iframe = $(this);
      var src = iframe.attr('src');
      var thumbnailURL = themeExtensions.getYouTubeThumbnailURL(src);
      if (thumbnailURL) {
        var slideIndex = $('.slick-dots li').length + 1;
        var thumbHTML = '<li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation' + slideIndex + '" id="slick-slide' + slideIndex + '" class="youtube" data-youtube="'+src+'">' +
                        '  <img src="' + thumbnailURL + '">' +
                        '</li>';
        prependHTML += thumbHTML;
      }
    });
    $('ul.slick-dots').append(prependHTML);
    $('.product-description .video-wrapper').remove();
    $('.product__slides').append('<div class="video-wrapper"></div>');

    $(document).on('click', 'ul.slick-dots li:not(.youtube), .product__slides button', function(evt) {
      evt.preventDefault();
      $('.product__slides .video-wrapper').hide();
      $('.product__slides .video-wrapper iframe').remove();
      // $('.featured-container .video-wrapper').hide();
      // $('.featured-container iframe').attr('src', '')
    });

    $(document).on('click', 'ul.slick-dots li.youtube', function(evt) {
      evt.preventDefault();

      var url = $(this).data('youtube');
      themeExtensions.playYTMobile(url);
    });
  },

  playYTMobile: function(url) {
    $('.product__slides .video-wrapper').show();
    var iframe = '<iframe src="' + url + '" frameborder="0" allowfullscreen=""></iframe>';
    $('.product__slides .video-wrapper').append(iframe);//.removeClass('featured-zoom');
  },

  getYouTubeThumbnailURL: function(url) {
    var identifier = themeExtensions.getYouTubeID(url);

    if (identifier) {
      // var url = 'https://img.youtube.com/vi/' + identifier + '/maxresdefault.jpg'
      var url = 'https://i.ytimg.com/vi/' + identifier + '/hqdefault.jpg'
      return url;
    }

    return null;
  },

  getYouTubeID: function(url) {
    var i = url.indexOf('?');
    if (i > -1) {
      url = url.substr(0, i);
    }

    i = url.indexOf('embed/');
    if (i > -1) {
      i += 'embed/'.length;
      return url.substr(i, url.length-i);
    }

    return null;
  },

  initVariantsPreview: function() {
    $(document).on('mouseenter', '.variants-preview a', function() {
      $(this).closest('.product-grid-item').find('.image-switch img').first().attr('src', $(this).find('span').data('image'));

    });
  },

  initTaxInfo: function() {
    if (window.location.pathname.indexOf('/cart') < 0) return;

    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(function(position) {
    //     console.log(position);
    //   });
    // }
  },

};

$(document).ready(function() {
  themeExtensions.initTabs();
  themeExtensions.initTableWrapper();
  themeExtensions.initMenuSearch();
  themeExtensions.initThumbPopups();
  themeExtensions.initVariantsPreview();
  themeExtensions.initProductVideo();
  themeExtensions.initTaxInfo();
});
