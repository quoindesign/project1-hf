

        jQuery(function() {
            var $module = jQuery('#m-1539774978372').children('.module');
            $module.gfV3Product();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978308').children('.module');
            var effect = $module.attr('data-effect');
            $module.gfV3ProductImage({
                'effect': effect
            })
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978314').children('.module');
            $module.gfV3ProductImageList({
                onImageClicked: function(imageUrl, imageZoomUrl) {}
            });

            var style = $module.attr('data-style');
            switch(style) {
                case 'slider':
                    var navspeed = $module.data('navspeed'),
                        navlg = $module.data('navlg'),
                        navmd = $module.data('navmd'),
                        navsm = $module.data('navsm'),
                        navxs = $module.data('navxs'),
                        collg = $module.data('collg'),
                        colmd = $module.data('colmd'),
                        colsm = $module.data('colsm'),
                        colxs = $module.data('colxs'),
                        dotslg = $module.data('dotslg'),
                        dotsmd = $module.data('dotsmd'),
                        dotssm = $module.data('dotssm'),
                        dotsxs = $module.data('dotsxs'),

                        marginlg = parseInt($module.data('mlg')),
                        marginmd = parseInt($module.data('mmd')),
                        marginsm = parseInt($module.data('msm')),
                        marginxs = parseInt($module.data('mxs'));

                    var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';
                    if(mode == 'production') {
                        var loop = $module.data('loop');
                    } else {
                        var loop = 0;
                    }
                    $module.find('.gf_product-images-list').owlCarousel({
                        mouseDrag: true,
                        navSpeed: navspeed,
                        autoWidth: !1,
                        loo: loop,
                        responsiveClass:true,
                        responsive:{
                            0:{
                                items:colxs,
                                nav: navxs,
                                dots:dotsxs,
                                margin: marginxs
                            },
                            768:{
                                items:colsm,
                                nav: navsm,
                                dots:dotssm,
                                margin: marginsm
                            },
                            992:{
                                items:colmd,
                                nav: navmd,
                                dots:dotsmd,
                                margin: marginmd
                            },
                            1200:{
                                items:collg,
                                nav: navlg,
                                dots:dotslg,
                                margin: marginlg
                            }
                        }
                    }); 
                    break;
            }
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978347').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978300').children('.module');
            $module.gfV3ProductPrice();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978369').children('.module');
            var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';

            var blankOption = $module.attr('data-blankoption');
            var blankOptionText = $module.attr('data-blankoptiontext');
            var style = $module.attr('data-style');

            $module.gfV3ProductVariants({
                mode: mode,
                blankOption: blankOption,
                blankOptionText: blankOptionText,
                style: style,
                onVariantSelected: function(variant, $select) {}
            });
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978355').children('.module');
            var style = $module.attr('data-style');
            var updatePrice = $module.attr('data-updateprice');

            $module.gfV3ProductQuantity({
                'style': style,
                'updatePrice': updatePrice
            });
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978344').children('.module');
            $module.gfV3ProductCartButton({ onItemAdded: function() {}});
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978278').children('.module');
            $module.gfV3ProductDesc();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1552340534688').children('.module');
            var activeTab = parseInt($module.attr('data-activeTab')) - 1;
            var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';

            $module.gfV3ResTabs({
                'activeTab': activeTab,
                'mode': mode,
                tabSelected: function(index) {
                    var tabInactiveBgColor = $module.attr('data-inactiveTabBgColor');
                    var tabInactiveColor = $module.attr('data-inactiveTabColor');
                    var tabBgColor = $module.attr('data-tabBgColor');
                    var tabColor = $module.attr('data-tabColor');
                    var borderColor = $module.attr('data-borderColor');
                    var borderSize = $module.attr('data-borderSize');

                    var $gfTabs = $module.find('>ul>li.gf_tab');
                    var $gfTabActive = $module.find('>ul>li.gf_tab-active');
                    var $gfTabPanels = $module.find('>.gf_tab-panel');

                    if($module.hasClass('style1')) {
                        $gfTabs
                        .css('margin-left', '-' + borderSize)
                        .css('border', borderSize + ' solid ' + borderColor)
                        .css('border-bottom', borderSize + ' solid ' + borderColor);

                        $module.find('>ul>li.gf_tab:first').css('margin-left', '0px');

                    } else if($module.hasClass('style3')) {
                        $gfTabs.css('border-bottom', borderSize + ' solid ' + tabBgColor);
                    }

                    if(!$module.hasClass('style3')) {
                        $gfTabs.css('background-color', tabInactiveBgColor);
                        $gfTabActive.css('background-color', tabBgColor);
                    }

                    $gfTabs.css('color', tabInactiveColor);
                    $gfTabActive.css('color', tabColor);

                    if($module.hasClass('style1')) {
                        $gfTabActive.children('.gf_tab-bottom')
                        .css('backgroundColor', tabBgColor)
                        .css('height', borderSize)
                        .css('bottom', '-' + borderSize);
                    } else if($module.hasClass('style3')) {
                        $gfTabActive.children('.gf_tab-bottom')
                        .css('backgroundColor', borderColor)
                        .css('height', borderSize)
                        .css('bottom', '-' + borderSize);
                    }
                    $gfTabPanels
                    .css('top', '-' + borderSize)
                    .css('background-color', tabBgColor)
                    .css('border', '1px solid ' + borderColor);

                    if($module.hasClass('style1')) {
                        $gfTabPanels.css('border', borderSize + ' solid ' + borderColor);
                    } else {
                        $gfTabPanels.css('border', 'none');
                    }
                }
            });
            if(mode == 'dev') {
                var moduleId = "1552340534688";
                var moduleIdSlug = moduleId.toString().replace(/-/g, '');
                if (moduleIdSlug == "1552340534688") {
                    window.getTab1552340534688 = function() {
                        return $module.data('gfv3restabs');
                    }
                }
            }
        });
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978364').children('.module');
        }); 
    
        jQuery(function() {
            var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';
            var $module = jQuery('#m-1552340651647').children('.module');
            if (mode == 'dev') {
                jQuery('#m-1552340651647').attr('data-name', '');
                jQuery('#m-1552340651647').css('background-image', 'none');
                jQuery('#m-1552340651647').css('min-height', '50px');
                jQuery('#m-1552340651647').removeAttr('data-image');
                
                var flag = true;
                var $bkLiquid = parent.jQuery('body').find('#gfFrame').contents().find('#module-1552340651647');
                if ($bkLiquid && $bkLiquid.length > 0) {
                    var $settings = $bkLiquid.find('.settings');
                    try {
                        var name = '';
                        var imageUrl = '';
                        settings = JSON.parse($settings.html());
                        for (var i = 0; i < settings.length; i++) {
                            if (settings[i].name == 'name') {
                                name = settings[i].default_value
                            }
                            if (settings[i].name == 'image') {
                                imageUrl = settings[i].default_value
                            }
                        }
                        if (imageUrl != '') {
                            flag = false;
                            jQuery('#m-1552340651647').css('background-image', 'url(' + imageUrl + ')');
                            jQuery('#m-1552340651647').css('min-height', '100px');
                            jQuery('#m-1552340651647').attr('data-image', 'true');
                        }
                        if (name != '' && name != 'Custom Code') {
                            flag = false;
                            jQuery('#m-1552340651647').attr('data-name', name);
                        }
                    } catch(error) {
                        console.log(error);
                    }
                }
                if (flag) {
                    jQuery('#m-1552340651647').attr('data-name', 'Right click on the module, then choose Edit Html / Liquid option to start writing your custom code.');
                }
            }

        }); 
    
        jQuery(function() {
            var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';
            var $module = jQuery('#m-1552340696347').children('.module');
            if (mode == 'dev') {
                jQuery('#m-1552340696347').attr('data-name', '');
                jQuery('#m-1552340696347').css('background-image', 'none');
                jQuery('#m-1552340696347').css('min-height', '50px');
                jQuery('#m-1552340696347').removeAttr('data-image');
                
                var flag = true;
                var $bkLiquid = parent.jQuery('body').find('#gfFrame').contents().find('#module-1552340696347');
                if ($bkLiquid && $bkLiquid.length > 0) {
                    var $settings = $bkLiquid.find('.settings');
                    try {
                        var name = '';
                        var imageUrl = '';
                        settings = JSON.parse($settings.html());
                        for (var i = 0; i < settings.length; i++) {
                            if (settings[i].name == 'name') {
                                name = settings[i].default_value
                            }
                            if (settings[i].name == 'image') {
                                imageUrl = settings[i].default_value
                            }
                        }
                        if (imageUrl != '') {
                            flag = false;
                            jQuery('#m-1552340696347').css('background-image', 'url(' + imageUrl + ')');
                            jQuery('#m-1552340696347').css('min-height', '100px');
                            jQuery('#m-1552340696347').attr('data-image', 'true');
                        }
                        if (name != '' && name != 'Custom Code') {
                            flag = false;
                            jQuery('#m-1552340696347').attr('data-name', name);
                        }
                    } catch(error) {
                        console.log(error);
                    }
                }
                if (flag) {
                    jQuery('#m-1552340696347').attr('data-name', 'Right click on the module, then choose Edit Html / Liquid option to start writing your custom code.');
                }
            }

        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978360').children('.module');
            var btnLabel = $module.attr('data-btnlabel');
            $module.gfSocialShare({
                btnLabel: btnLabel
            });
        });
    
        jQuery(function() {
            var $hero = jQuery('#m-1552340714099');
            var $module = jQuery('#m-1552340714099').children('.module');

            var mode = jQuery('.gryffeditor').hasClass('editing') ? 'dev' : 'production';
            var $heroLink = $hero.children('.hero-link');
            if(mode == 'dev' && $heroLink.length > 0) {
                 $hero.children('.hero-link').hide();
            }
            if (mode == 'production' && $heroLink.length > 0) {
                $module.off('click.openLink').on('click.openLink', function(e) {
                    var $target = jQuery(e.target);
                    if ($target.length > 0){
                        var linkTarget = typeof $target.attr('href') !== "undefined" ? $target.attr('href') : "";
                        if (linkTarget == "") {
                            var link = typeof $heroLink.attr('href') !== "undefined" ? $heroLink.attr('href') : "";
                            var newTab = typeof $heroLink.attr('target') !== "undefined" ? $heroLink.attr('target') : "";
                            if (link != "") {
                                if (newTab == "") {
                                    window.location.href = link;
                                } else {
                                    window.open(link, newTab);
                                }
                            }
                        }
                    };
                })
            }

            var height = jQuery.trim($module.attr('data-height'));
            if(height == undefined || height == '') {
                $hero.attr('style', 'height: auto!important');
                jQuery(window).resize(function(){
                    $hero.attr('style', 'height: auto!important');
                });
            } else {
                $hero.removeAttr('style');
            }

            var effect = $module.attr('data-effect');
            var transition = $module.attr('data-transition');

            if(effect == 'effect-zoom') {   
                $module.parent().addClass(effect);  

                setTimeout(function() {
                    var backgroundImage = $module.parent().css('background-image');
                    var backgroundSize = $module.parent().css('background-size');
                    var backgroundPosition = $module.parent().css('background-position');
                    $module.siblings('.gf_hero-bg-wrap').css({
                        'background-image'      : 'inherit',
                        'background-size'       : 'inherit',
                        'background-position'   : 'inherit',
                        '-webkit-transition'    : 'transform ' + transition + 's ease-in-out',
                        '-moz-transition'       : 'transform ' + transition + 's ease-in-out',
                        '-ms-transition'        : 'transform ' + transition + 's ease-in-out',
                        '-o-transition'         : 'transform ' + transition + 's ease-in-out',
                        'transition'            : 'transform ' + transition + 's ease-in-out'
                    })
                    $module.siblings('.gf_hero-bg-wrap').children('.gf_hero-bg').css({
                        'background-image'      : 'inherit',
                        'background-size'       : 'inherit',
                        'background-position'   : 'inherit',
                        '-webkit-transition'    : 'transform ' + transition + 's ease-in-out',
                        '-moz-transition'       : 'transform ' + transition + 's ease-in-out',
                        '-ms-transition'        : 'transform ' + transition + 's ease-in-out',
                        '-o-transition'         : 'transform ' + transition + 's ease-in-out',
                        'transition'            : 'transform ' + transition + 's ease-in-out'
                    });
                }, 300);
            }

            if($module.attr('data-fixedMode') == '1'){
                $module.parent().attr('style', 'padding-top: 0px!important; padding-bottom: 0px!important; height: auto!important; background-image: none!important;max-width: 100%!important;');

                jQuery(window).resize(function(){
                    var backgroundImage =  $module.parent().css('background-image');
                    $module.parent().attr('style', 'padding-top: 0px!important; padding-bottom: 0px!important; height: auto!important; background-image: none!important;max-width: 100%!important;');
                });
            } else {
                $module.parent().removeAttr('style');
            }
        });
    
        jQuery(function() {
            var $module = jQuery('#m-1552340824717').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978349').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978243').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978252').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978287').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247').children('.module');

            var limit = parseInt($module.attr('data-limit'));
            if($module.find('.gf_row.gf_row_no_tools .gf_column').length > limit) {
                $module.find('.gf_row.gf_row_no_tools .gf_column:last').hide();
            }
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child1').children('.module');
            $module.gfV3Product();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child1-4').children('.module');
            var effect = $module.attr('data-effect');
            $module.gfV3ProductImage({
                'effect': effect
            })
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child1-6').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child1-5').children('.module');
            $module.gfV3ProductPrice();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child2').children('.module');
            $module.gfV3Product();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child2-4').children('.module');
            var effect = $module.attr('data-effect');
            $module.gfV3ProductImage({
                'effect': effect
            })
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child2-6').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child2-5').children('.module');
            $module.gfV3ProductPrice();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child3').children('.module');
            $module.gfV3Product();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child3-4').children('.module');
            var effect = $module.attr('data-effect');
            $module.gfV3ProductImage({
                'effect': effect
            })
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child3-6').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child3-5').children('.module');
            $module.gfV3ProductPrice();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child4').children('.module');
            $module.gfV3Product();
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child4-4').children('.module');
            var effect = $module.attr('data-effect');
            $module.gfV3ProductImage({
                'effect': effect
            })
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child4-6').children('.module');
        }); 
    
        jQuery(function() {
            var $module = jQuery('#m-1539774978247-child4-5').children('.module');
            $module.gfV3ProductPrice();
        }); 
    